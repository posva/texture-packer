#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <vector>



class SpriteAtlas : public sf::Sprite {
	size_t myTextureIndex; //Index in the texture table
	double myArea;
public:
    inline SpriteAtlas* me() { return this; }
	SpriteAtlas() {}
	~SpriteAtlas() {}
	
	void setTexture(const std::vector<sf::Texture>& textures, size_t index)
	{
		sf::Sprite::setTexture(textures[index]);
		myTextureIndex = index;
		
		myArea = getTexture()->getSize().x * getTexture()->getSize().y;
	}
	
	inline bool operator<(const SpriteAtlas& other) const
	{
		return (myArea > other.myArea);
		/*
		if (getTextureRect().height != other.getTextureRect().height)
			return (getTextureRect().height > other.getTextureRect().height); //Hack pour utiliser la fonction std::list.sort()
		else
			return (getTextureRect().width > other.getTextureRect().width);*/
	}
	
	inline double getArea() const { return myArea; }
	inline size_t getTextureIndex() const { return myTextureIndex; }
	inline bool intersects(const SpriteAtlas& other) const
	{
		sf::FloatRect me(getTextureRect()), him(other.getTextureRect());
		me.left = getPosition().x; me.top = getPosition().y;
		him.left = other.getPosition().x; him.top = other.getPosition().y;
		return me.intersects(him);
	}
	
	sf::FloatRect getRectangle() const
	{
		sf::FloatRect rect(getTextureRect());
		rect.top = getPosition().y;
		rect.left = getPosition().x;
		
		return rect;
	}
};
