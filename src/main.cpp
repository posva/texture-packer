#include <SFML/Graphics.hpp>
#include <iostream>
#include <dirent.h>
#include "SpriteAtlas.hpp"
#include "tinyxml2.hpp"
#include "IniFile.hpp"
#include <list>
#include <cmath>
#include <sstream>

int getFiles(const std::string& dir, std::vector<std::string> &files, const char* ext);

void setPositions(std::list<SpriteAtlas>& sprites, double bestAprox, sf::RenderWindow* window = NULL);

void setMethod2(std::list<SpriteAtlas>& sprites, double bestAprox, sf::RenderWindow* window = NULL);

void setMethodSlow(std::list<SpriteAtlas>& sprites, double bestAprox, float offset, sf::RenderWindow* window = NULL);

void createIniFile(std::list<SpriteAtlas>& sprites, const std::vector<std::string>& texturesFiles, const std::string& file);

void createXMLFile(std::list<SpriteAtlas*>& sprites, const std::vector<std::string>& texturesFiles, const std::string& file);


int main (int argc, const char * argv[])
{
	if (argc < 2)
	{
		std::cout<<"Usage: ./"<<argv[0]<<" <directory containing the png files>\n";
		return 1;
	}
	
	std::string wd(argv[1]); //working directory
	if (wd[wd.size()-2] != '/')
		wd.push_back('/');
	
	std::vector<std::string> texturesFiles;
	std::vector<sf::Texture> textures;
	std::list<SpriteAtlas> sprites;
    std::list<SpriteAtlas*> sprites_ordered;
	getFiles(wd, texturesFiles, ".png");
	
	if (texturesFiles.size() <= 0)
	{
		std::cout<<"No png files found\n";
		return 1;
	}
	
	textures.resize(texturesFiles.size());
	sprites.resize(texturesFiles.size());
	
	std::list<SpriteAtlas>::iterator sprites_it(sprites.begin());
	double maxSize(0.0), totalArea(0.0);
	float offset(0.f);
	//le offset sépare les images d'un certain nombre de pixels
	for (unsigned int i=0; i<texturesFiles.size(); ++i)
	{
		std::cout<<"Loading texture for file: "<<wd + texturesFiles[i]<<std::endl;
		textures[i].loadFromFile(wd + texturesFiles[i]);
		(*sprites_it).setTexture(textures, i);
		totalArea += (*sprites_it).getArea() + sprites_it->getTexture()->getSize().x*offset + sprites_it->getTexture()->getSize().y*offset;
		++sprites_it;
		
		if (textures[i].getSize().x > maxSize)
			maxSize = textures[i].getSize().x;
		if (textures[i].getSize().y > maxSize)
			maxSize = textures[i].getSize().y;
	}
	
	double bestAprox(1.0); // 2^0
	
	//we find out the need area for the big texture
	while (bestAprox*bestAprox < totalArea || bestAprox < maxSize) {
		bestAprox *= 2.0;
	}
	
	//createXMLFile(sprites, texturesFiles, "file.xml");
	
	std::cout<<"Sorting textures by area...\n";
    for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
        sprites_ordered.push_back(it->me());
    
	sprites.sort(); //We sort by area yo have bigger textures first
	std::cout<<"Max area: "<<sprites.front().getArea()<<"\nMin area: "<<sprites.back().getArea()<<"\nTotal area needed: "<<totalArea<<"\nBest aproximation size (square): "<<bestAprox<<"\nUnused space: "<<bestAprox*bestAprox-totalArea<<"\n";
	
#ifdef DEBUG
	//output area for each sprite
	unsigned int i(0);
	for (sprites_it = sprites.begin(); sprites_it != sprites.end(); ++sprites_it)
		std::cout<<i<<": "<<texturesFiles[(*sprites_it).getTextureIndex()]<<"; Area: "<<(*sprites_it).getArea()<<std::endl, ++i;
#endif
	
	
	
	
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
	
	sf::Clock clock;
	//setPositions(sprites, bestAprox, NULL);
	clock.restart();
#ifdef DEBUG
	setMethodSlow(sprites, bestAprox, offset, &window);
#else
	setMethodSlow(sprites, bestAprox, offset, NULL);
#endif
	std::cout<<"Time: "<<clock.getElapsedTime().asSeconds()<<"\n";
	
	//we check that the job is well done
	bool success(true);
	for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
	{
		for (std::list<SpriteAtlas>::iterator it2 = it; it2 != sprites.end(); ++it2)
			if (it != it2 && (*it).intersects(*it2))
			{
				success = false;
				std::cout<<"Conflict at "<<(*it).getPosition().x<<", "<<(*it).getPosition().y<<"\n";
			}
	}
	
	if (success)
		std::cout<<"The generated texture is valid!\n";
	else
		std::cout<<"Sorry, the genrated texture is invalid :(\n";
	
	createIniFile(sprites, texturesFiles, "file.ini");
    
    createXMLFile(sprites_ordered, texturesFiles, "file.xml");
	
	sf::RenderTexture tx;
	tx.create(bestAprox, bestAprox);
	
	tx.clear(sf::Color(0,0,0,0));
	for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
		tx.draw(*it);
	tx.display();
	
	tx.getTexture().copyToImage().saveToFile("Texturepack.png");

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
				window.close();
		}

		window.clear(sf::Color(50,50,50));
		
		for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
			window.draw(*it);

		window.display();
	}
	 
	
	textures.clear();
	texturesFiles.clear();
	sprites.clear();

	return EXIT_SUCCESS;
}


int getFiles(const std::string& dir, std::vector<std::string> &files, const char* ext)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        printf("Cannot get acces to dir\n");
        return 1;
    }
	
	files.clear();
	
    while ((dirp = readdir(dp)) != NULL) {
		if (strstr(dirp->d_name, ext) != NULL)
			files.push_back(std::string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

void setPositions(std::list<SpriteAtlas>& sprites, double bestAprox, sf::RenderWindow* window)
{
	//We position the sprites
	std::list<SpriteAtlas>::iterator tmp_it;
	std::list<SpriteAtlas>::iterator sprites_it;
	double maxLineHeight(0.0); //Max height of the working line because we  will try to move vertically before moving horizontally
	double  minColumnWidth(0.0);
	float currentLine(0.f), currentColumn(0.f);
	unsigned int count(0);
	
	for (sprites_it = sprites.begin(); sprites_it != sprites.end(); ++sprites_it)
	{
		SpriteAtlas& spr(*sprites_it);
		currentLine = 0.f;
		currentColumn = 0.f;
		SpriteAtlas *leftspr;
		for (tmp_it = sprites.begin(); tmp_it !=sprites_it; ++tmp_it) // we check rctangle collision with sprites that have been already positioned 
		{
			SpriteAtlas& other(*tmp_it);
			if (currentLine == other.getPosition().y)
				leftspr = &other;
			
			while (spr.intersects(other))
			{
				if (window)
				{
				 sf::FloatRect r1(spr.getRectangle()), r2(other.getRectangle());
				 std::cout<<"spr: "<<r1.left<<","<<r1.top<<": "<<r1.width<<", "<<r1.height<<"\n";
				 std::cout<<"other: "<<r2.left<<","<<r2.top<<": "<<r2.width<<", "<<r2.height<<"\n";
				 std::cout<<"Sprite "<<count<<" need to be moved\n";
				}
				//we retrieve the maxLineHeight
				maxLineHeight = 0.0;
				for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites_it; ++it)
				{
					
					if ((*it).getPosition().y == currentLine && (*it).getTextureRect().height > maxLineHeight)
						maxLineHeight = (*it).getTextureRect().height;
					
				}
				if (window)
					std::cout<<"Max Height for the line "<<currentLine<<": "<<maxLineHeight<<"\n";
				
				//we retrieve the minColumnWidth
				minColumnWidth = 0.f;
				 for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites_it; ++it)
				 {
				 
				 if ((*it).getPosition().x == currentColumn && (minColumnWidth == 0.f || (*it).getTextureRect().width < minColumnWidth))
				 minColumnWidth = (*it).getTextureRect().width;
				 
				 }
				//std::cout<<"Min width for the column "<<currentColumn<<": "<<minColumnWidth<<"\n";
				
				//we first try to move vertically
				float move(other.getPosition().y+other.getTextureRect().height - spr.getPosition().y+1.f);
				//std::cout<<"To move vert should be moved of "<<move<<"\n";
				if (spr.getPosition().x + spr.getTextureRect().width <= bestAprox) //we got out of the line
				{
					if (window)
						std::cout<<"We move hori\n";
					currentColumn = leftspr->getPosition().x + leftspr->getTextureRect().width + 1.f;
					spr.setPosition(currentColumn, currentLine); //we move horizontally and we go to the top again and we need to check again with everyone
					tmp_it = --sprites.begin();
				}
				else if (!(move + spr.getTextureRect().height +spr.getPosition().y > currentLine + maxLineHeight))
				{
					if (window)
						std::cout<<"We move vert\n";
					spr.move(0.f, move); // we move vertically to not collide anymore
										 //tmp_it = --sprites.begin();
				}
				
				
				if (spr.getPosition().x + spr.getTextureRect().width > bestAprox) //we are outside the texture
				{
					currentLine += maxLineHeight + 1.f;
					currentColumn = 0.f;
					spr.setPosition(0.f, currentLine);
					if (window)
						std::cout<<"Go the a new line\n";
				}
				
				if (window != NULL)
				{
					window->clear(sf::Color(50,50,50));
					
					
					spr.setColor(sf::Color::Blue);
					other.setColor(sf::Color::Blue);
					
					for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
						window->draw(*it);
					
					window->display();
					
					spr.setColor(sf::Color::White);
					other.setColor(sf::Color::White);
					sf::Event event;
					
					while (window->waitEvent(event))
					{
						if (event.type == sf::Event::KeyPressed)
							break;
					}
				}
				 
				 
			}
			
		}
		//we're placed go ahead with the next sprite!
		if (window)
			std::cout<<"Placed texture "<<count<<" at "<<spr.getPosition().x<<", "<<spr.getPosition().y<<std::endl;
		++count;
		
	}

}

void setMethod2(std::list<SpriteAtlas>& sprites, double bestAprox, sf::RenderWindow* window)
{
	sf::Vector2f cursor, line_cursor;
	float maxHeight, maxWidth;
	for (std::list<SpriteAtlas>::iterator actual_it = sprites.begin(); actual_it != sprites.end(); ++actual_it)
	{
		SpriteAtlas& actual_spr(*actual_it);
		
		line_cursor = sf::Vector2f();
		cursor = sf::Vector2f();
		
		for (std::list<SpriteAtlas>::iterator comp_it = sprites.begin(); comp_it != actual_it; ++comp_it)
		{
			SpriteAtlas& comp_spr(*comp_it);
			
			while (actual_spr.intersects(comp_spr))
			{
				//we got max heigth of current line. As they are ordered by height the first in line has the bigger heigt
				maxHeight = 0.f;
				for (std::list<SpriteAtlas>::iterator tmp_it = sprites.begin(); tmp_it != actual_it; ++tmp_it)
				{
					if (line_cursor.y == (*tmp_it).getPosition().y && (*tmp_it).getTextureRect().height > maxHeight)
						maxHeight = (*tmp_it).getTextureRect().height;
				}
				//we got maxwidth for the current column
				maxWidth = 0.f;
				for (std::list<SpriteAtlas>::iterator tmp_it = sprites.begin(); tmp_it != actual_it; ++tmp_it)
				{
					if (line_cursor.x == (*tmp_it).getPosition().x && (*tmp_it).getTextureRect().width > maxWidth)
						maxWidth = (*tmp_it).getTextureRect().width;
				}
				
				//we first try to move vertically
				float move(comp_spr.getPosition().y+comp_spr.getTextureRect().height - actual_spr.getPosition().y+1.f);
				if (move + actual_spr.getTextureRect().height + actual_spr.getPosition().y <= line_cursor.y + maxHeight) //we can move vertically so we do it
				{
					actual_spr.move(0.f, move);
					//And we can go check with the next sprite
				}
				else //we are going to try to move horizontally
				{
					
				}
			
			}
				
		}
	}
}

void setMethodSlow(std::list<SpriteAtlas>& sprites, double bestAprox, float offset, sf::RenderWindow* window)
{
	//int offset(0);
	for (std::list<SpriteAtlas>::iterator actual_it = sprites.begin(); actual_it != sprites.end(); ++actual_it)
	{
		SpriteAtlas& actual_spr(*actual_it);
		
		for (std::list<SpriteAtlas>::iterator comp_it = sprites.begin(); comp_it != actual_it; ++comp_it)
		{
			SpriteAtlas& comp_spr(*comp_it);
			bool valid(1);

			actual_spr.setPosition(comp_spr.getPosition().x + comp_spr.getTextureRect().width+static_cast<float>(offset), comp_spr.getPosition().y);
			
			for (std::list<SpriteAtlas>::iterator comp_it2 = sprites.begin(); comp_it2 != actual_it; ++comp_it2)
				if (actual_spr.intersects(*comp_it2))
					valid = false;
			
			if (!valid || actual_spr.getPosition().x + actual_spr.getTextureRect().width > bestAprox || actual_spr.getPosition().y + actual_spr.getTextureRect().height > bestAprox)
			{
				actual_spr.setPosition(comp_spr.getPosition().x, comp_spr.getPosition().y+comp_spr.getTextureRect().height+static_cast<float>(offset));
				
				
				valid = 1;
				for (std::list<SpriteAtlas>::iterator comp_it2 = sprites.begin(); comp_it2 != actual_it; ++comp_it2)
					if (actual_spr.intersects(*comp_it2))
						valid = false;
			}
			
			if (valid && (actual_spr.getPosition().x + actual_spr.getTextureRect().width > bestAprox || actual_spr.getPosition().y + actual_spr.getTextureRect().height > bestAprox))
				valid = 0;
				
			
			
			
			if (window != NULL)
			{
				window->clear(sf::Color(50,50,50));
				
				
				actual_spr.setColor(sf::Color::Blue);
				comp_spr.setColor(sf::Color::Blue);
				
				for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
					window->draw(*it);
				
				window->display();
				
				actual_spr.setColor(sf::Color::White);
				comp_spr.setColor(sf::Color::White);
				sf::Event event;
				
				while (0)//window->waitEvent(event))
				{
					if (event.type == sf::Event::KeyPressed)
						break;
				}
			}
			
			if (valid)
				break;

			
		}
		
	}
}

void createXMLFile(std::list<SpriteAtlas*>& sprites, const std::vector<std::string>& texturesFiles, const std::string& file)
{
	tinyxml2::XMLDocument doc;
	tinyxml2::XMLNode* animations(doc.InsertFirstChild(doc.NewElement("animations")));
	animations->ToElement()->SetAttribute("texture", "NAME_ME");
	animations->ToElement()->SetAttribute("key", "KEY");
	
	tinyxml2::XMLNode* sprite, *frame;
	
	std::map<std::string, tinyxml2::XMLNode*> anims;
	
	std::list<SpriteAtlas*>::iterator it = sprites.begin();
	for (int i(0); i < texturesFiles.size(); ++i)
	{
		size_t strip_pos = std::string::npos;
		unsigned int stripCount(0);
		int index(-1), read;
		strip_pos = texturesFiles[i].find("strip");
		
		if ( strip_pos != std::string::npos) // c'est une animation avec plusieurs frames
		{
			read = sscanf(texturesFiles[i].substr(strip_pos).c_str(), "strip%u_%d.png", &stripCount, &index);
			std::cout<<i<<": "<<texturesFiles[i]<<"("<<read<<") = png: "<<texturesFiles[i].substr(0, strip_pos-1)<<", sc: "<<stripCount<<", i: "<<index<<"\n";
			
			if (index == 0) // premier, il faut créer sprite
			{
				sprite = animations->InsertEndChild(doc.NewElement("sprite"));
				sprite->ToElement()->SetAttribute("name", texturesFiles[i].substr(0, strip_pos-1).c_str());
				sprite->ToElement()->SetAttribute("originx", (*it)->getTextureRect().width/2.f);
				sprite->ToElement()->SetAttribute("originy", (*it)->getTextureRect().height/2.f);
				sprite->ToElement()->SetAttribute("interval", 15);
				if (stripCount >= 10) // il y a un désordre dans la liste
					sprite->ToElement()->SetAttribute("count", stripCount);
				// on le met dans la map pour l'utiliser plus tard
				anims[texturesFiles[i].substr(0, strip_pos-1)] = sprite;
			}
			else
			{
				// il faut ajouter a un tag sprite deja existant
				sprite = anims[texturesFiles[i].substr(0, strip_pos-1)];
			}
			// puis le frame qui lui est propre
			frame = sprite->InsertEndChild(doc.NewElement("frame"));
			frame->ToElement()->SetAttribute("left", (*it)->getPosition().x);
			frame->ToElement()->SetAttribute("top", (*it)->getPosition().y);
			frame->ToElement()->SetAttribute("width", (*it)->getTextureRect().width);
			frame->ToElement()->SetAttribute("height", (*it)->getTextureRect().height);
			if (stripCount >= 10)
				frame->ToElement()->SetAttribute("index", index);
		}
		else
		{
			// Ajout d'un sprite simple avec une seule frame
			sprite = animations->InsertEndChild(doc.NewElement("sprite"));
			sprite->ToElement()->SetAttribute("name", texturesFiles[i].substr(0, texturesFiles[i].find(".png")).c_str());
			sprite->ToElement()->SetAttribute("originx", (*it)->getTextureRect().width/2.f);
			sprite->ToElement()->SetAttribute("originy", (*it)->getTextureRect().height/2.f);
			frame = sprite->InsertEndChild(doc.NewElement("frame"));
			frame->ToElement()->SetAttribute("left", (*it)->getPosition().x);
			frame->ToElement()->SetAttribute("top", (*it)->getPosition().y);
			frame->ToElement()->SetAttribute("width", (*it)->getTextureRect().width);
			frame->ToElement()->SetAttribute("height", (*it)->getTextureRect().height);
		}
		++it;
	}
	
	
	doc.SaveFile(file.c_str());
}

void createIniFile(std::list<SpriteAtlas>& sprites, const std::vector<std::string>& texturesFiles, const std::string& file)
{
	IniFile ini;
	std::ostringstream stream;
	
	for (std::list<SpriteAtlas>::iterator it = sprites.begin(); it != sprites.end(); ++it)
	{
		ini.addSection(texturesFiles[(*it).getTextureIndex()]);
		stream<<(*it).getPosition().x;
		ini.addValue(texturesFiles[(*it).getTextureIndex()], "x", stream.str());
		stream.str("");
		stream<<(*it).getPosition().y;
		ini.addValue(texturesFiles[(*it).getTextureIndex()], "y", stream.str());
		stream.str("");
		stream<<(*it).getTextureRect().width;
		ini.addValue(texturesFiles[(*it).getTextureIndex()], "w", stream.str());
		stream.str("");
		stream<<(*it).getTextureRect().height;
		ini.addValue(texturesFiles[(*it).getTextureIndex()], "h", stream.str());
		stream.str("");
	}
	
	ini.save(file);
}
